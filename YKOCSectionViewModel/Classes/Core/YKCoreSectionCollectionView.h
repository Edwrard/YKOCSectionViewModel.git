//
//  YKCoreSectionCollectionView.h
//  YK_SectionViewModel
//
//  Created by edward on 2021/11/26.
//  Copyright © 2021 YK_SectionViewModel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKCoreSectionViewModelMainProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@interface YKCoreSectionCollectionView : UICollectionView


/// 默认超时时间
@property (nonatomic, assign, readwrite) CGFloat outTime;
/// 错误回调
@property (nonatomic, strong, readwrite) void(^errorCallBack)(NSError *error);
/// 结束刷新动作回调
@property (nonatomic, strong, readwrite) void(^endRefresh)(void);
/// 处理跳转使用方式
@property (nonatomic, strong, readwrite) void(^handleViewController)(UIViewController *controller, YKCoreSectionViewModelPushType type, BOOL animated);
/// loading回调,现在均只有在所有viewmodel中 reloadBlock()被执行完才会返回flas
@property (nonatomic, strong, readwrite) void(^loadingCallBack)(BOOL isLoading);

/// 初始化
/// @param frame 布局
/// @param datas viewmodels
- (instancetype)initWithFrame:(CGRect)frame datas:(NSArray<id<YKCoreSectionViewModelMainProtocol>> *)datas;

/// 重新加载
/// @param mode 加载模式
- (void)refreshDataWithMode:(YKCoreSectionViewModelRefreshMode)mode;

/// 重新设置数据源
/// @param viewModels viewModels
- (void)resetViewModels:(NSArray<id<YKCoreSectionViewModelMainProtocol>> *)viewModels;

/// 响应动作
/// @param eventName 动作名称
/// @param userInfo 参数
- (BOOL)handleRouterEvent:(NSString *)eventName userInfo:(NSDictionary *)userInfo;

/// 设置没有内容时显示的提示
/// @param tip 提示
/// @param font 字体
- (void)setNoDataViewTip:(NSString *)tip font:(UIFont *)font;

/// 设置无内容时的图片
/// @param image 图片
- (void)setNoDataViewImage:(UIImage *)image;

@end

NS_ASSUME_NONNULL_END
