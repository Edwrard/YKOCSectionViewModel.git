//
//  YKCoreSectionViewModelMainProtocol.h
//  YK_SectionViewModel
//
//  Created by edward on 2021/11/27.
//  Copyright © 2021 YK_SectionViewModel. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "YKCoreSectionSectionResuseModel.h"

NS_ASSUME_NONNULL_BEGIN

typedef NS_ENUM(NSInteger, YKCoreSectionViewModelPushType) {
    YKCoreSectionViewModelPushTypePush,
    YKCoreSectionViewModelPushTypePresent,
};

typedef NS_ENUM(NSInteger, YKCoreSectionViewModelRefreshMode) {
    YKCoreSectionViewModelRefreshModeHeader,
    YKCoreSectionViewModelRefreshModeFooter,
};


@protocol YKCoreSectionViewModelMainProtocol <NSObject>

@required

/// 获取当前section的item数量
- (NSInteger)yksc_numberOfItem;

/// 刷新页面的时候触发
/// @param mode 触发模式，下拉刷新或上拉加载
/// @param reloadBlock 刷新页面
/// @param errorBlock 错误回调
- (void)yksc_beignToReloadDataWithMode:(YKCoreSectionViewModelRefreshMode)mode reloadBlock:(void(^)(void))reloadBlock errorBlock:(void(^)(NSError *error))errorBlock;

/// 当前索引item id
/// @param indexPath 当前索引
- (NSString *)yksc_idForItemAtIndexPath:(NSIndexPath *)indexPath;

/// 注册所用到的cell的id
- (NSArray<YKCoreSectionSectionResuseModel*> *)yksc_registItems;

/// 获取索引cell的大小
/// @param width 宽度
/// @param indexPath 索引
- (CGSize)yksc_sizeOfItemWithWidth:(CGFloat)width AtIndexPath:(NSIndexPath *)indexPath;

@optional

/// 当前section无数据时是否显示头部和底部
- (BOOL)yksc_noDataShowHeaderFooter;

/// 注册头部信息
- (NSArray<YKCoreSectionSectionResuseModel *> *)yksc_registHeaders;

/// 获取头部Id
- (NSString *)yksc_idForHeader;

/// 头部大小
/// @param width 宽度
- (CGSize)yksc_sizeOfHeaderWithWidth:(CGFloat)width;

/// 底部信息
- (NSArray<YKCoreSectionSectionResuseModel *> *)yksc_registFooters;

/// 获取底部Id
- (NSString *)yksc_idForFooter;

/// 底部大小
/// @param width 宽度
- (CGSize)yksc_sizeOfFooterWithWidth:(CGFloat)width;

/// 当前最小间距
- (CGFloat)yksc_sectionMinimumLineSpacing;

/// 当前最小行间距
- (CGFloat)yksc_sectionMinimumInteritemSpacing;

/// 被点击时触发
/// @param indexPath 当前索引
/// @param callBack callback回调
- (void)yksc_didSelectItemAtIndexPath:(NSIndexPath *)indexPath collectionView:(UICollectionView *)collectionView callBack:(void(^)(UIViewController *viewcontroller,YKCoreSectionViewModelPushType type, BOOL animated))callBack;

/// 响应内容
/// @param eventName 内容名称
/// @param userInfo 内容附赠内容
/// @param controllerEvent 回调
- (BOOL)yksc_handleRouterEvent:(NSString *)eventName collectionView:(UICollectionView *)collectionView userInfo:(NSDictionary *)userInfo controllerEvent:(void(^)(UIViewController *controller, YKCoreSectionViewModelPushType type, BOOL animated))controllerEvent;

@end

NS_ASSUME_NONNULL_END
