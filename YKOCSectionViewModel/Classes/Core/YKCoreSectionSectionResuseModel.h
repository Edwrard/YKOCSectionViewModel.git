//
//  YKCoreSectionSectionResuseModel.h
//  YK_SectionViewModel
//
//  Created by linghit on 2021/12/6.
//  Copyright © 2021 YK_SectionViewModel. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKCoreSectionSectionResuseModel : NSObject

/// View的类名
@property (nonatomic, copy, readonly) NSString *className;

/// View的id
@property (nonatomic, copy, readonly) NSString *Id;

+ (instancetype)modelWithClassName:(NSString *)className Id:(NSString *)Id;
@end

NS_ASSUME_NONNULL_END
