//
//  YKNewSectionSectionResuseViewModel.m
//  YK_SectionViewModel
//
//  Created by linghit on 2021/12/6.
//  Copyright © 2021 YK_SectionViewModel. All rights reserved.
//

#import "YKCoreSectionSectionResuseModel.h"

@interface YKCoreSectionSectionResuseModel ()

/// View的类名
@property (nonatomic, copy, readwrite) NSString *className;

/// View的id
@property (nonatomic, copy, readwrite) NSString *Id;
@end

@implementation YKCoreSectionSectionResuseModel

+ (instancetype)modelWithClassName:(NSString *)className Id:(NSString *)Id
{
    YKCoreSectionSectionResuseModel *model = [[YKCoreSectionSectionResuseModel alloc] init];
    model.className = className;
    model.Id = Id;
    return model;
}

@end
