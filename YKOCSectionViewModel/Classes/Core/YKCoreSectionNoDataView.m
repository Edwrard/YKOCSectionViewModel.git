//
//  YKCoreSectionNoDataView.m
//  YK_SectionViewModel
//
//  Created by linghit on 2021/12/9.
//

#import "YKCoreSectionNoDataView.h"

@interface YKCoreSectionNoDataView ()
///
@property (nonatomic, strong, readwrite) UIImageView *errorImageView;
///
@property (nonatomic, strong, readwrite) UILabel *tipLabel;
///
@property (nonatomic, strong, readwrite) UIButton *refreshButton;
@end

@implementation YKCoreSectionNoDataView

- (instancetype)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if (self) {
        self.backgroundColor = UIColor.whiteColor;
        self.errorImageView.frame = CGRectMake(((self.bounds.size.width - 100))/2, (self.bounds.size.height - 350)/2, 100, 100);
        self.tipLabel.frame = CGRectMake(0, CGRectGetMaxY(self.errorImageView.frame) + 30, self.bounds.size.width, 30);
        self.refreshButton.frame = CGRectMake((self.bounds.size.width - 100)/2, CGRectGetMaxY(self.tipLabel.frame) + 10, 100, 30);
        [self addSubview:self.errorImageView];
        [self addSubview:self.tipLabel];
        [self addSubview:self.refreshButton];
    }
    return self;
}

- (void)reloadClick:(id)sender
{
    if (self.reloadCallBack) {
        self.reloadCallBack();
    }
}

- (UIImageView *)errorImageView
{
    if (!_errorImageView) {
        _errorImageView = [[UIImageView alloc] init];
        _errorImageView.image = [UIImage imageNamed:@"ic_sectionvm_nodata_image"];
    }
    return _errorImageView;
}

- (UILabel *)tipLabel
{
    if (!_tipLabel) {
        _tipLabel = [[UILabel alloc] init];
        _tipLabel.text = @"暂无相关数据";
        _tipLabel.textAlignment = NSTextAlignmentCenter;
        _tipLabel.font = [UIFont systemFontOfSize:16];
        _tipLabel.numberOfLines = 0;
        [_tipLabel sizeToFit];
    }
    return _tipLabel;
}

- (UIButton *)refreshButton
{
    if (!_refreshButton) {
        _refreshButton = [UIButton buttonWithType:UIButtonTypeCustom];
        [_refreshButton setTitle:@"点击刷新" forState:UIControlStateNormal];
        [_refreshButton setTitleColor:UIColor.blueColor forState:UIControlStateNormal];
        _refreshButton.titleLabel.font = [UIFont systemFontOfSize:16];
        [_refreshButton addTarget:self action:@selector(reloadClick:) forControlEvents:UIControlEventTouchUpInside];
    }
    return _refreshButton;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/

@end
