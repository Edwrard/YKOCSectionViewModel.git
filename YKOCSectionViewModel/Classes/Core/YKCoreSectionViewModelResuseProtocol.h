//
//  YKCoreSectionViewModelResuseProtocol.h
//  YK_SectionViewModel
//
//  Created by edward on 2021/12/6.
//  Copyright © 2021 YK_SectionViewModel. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "YKCoreSectionViewModelMainProtocol.h"

NS_ASSUME_NONNULL_BEGIN

@protocol YKCoreSectionViewModelResuseProtocol <NSObject>

@required

/// cell加载数据
/// @param viewModel 当前viewmodel
/// @param indexPath 索引
- (void)loadDataWithViewModel:(id<YKCoreSectionViewModelMainProtocol>)viewModel atIndexPath:(NSIndexPath *)indexPath;

@end

NS_ASSUME_NONNULL_END
