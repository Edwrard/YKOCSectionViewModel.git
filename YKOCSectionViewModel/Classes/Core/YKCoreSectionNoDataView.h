//
//  YKCoreSectionNoDataView.h
//  YK_SectionViewModel
//
//  Created by linghit on 2021/12/9.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKCoreSectionNoDataView : UIView

///
@property (nonatomic, strong, readonly) UIImageView *errorImageView;
///
@property (nonatomic, strong, readonly) UILabel *tipLabel;

///
@property (nonatomic, copy, readwrite) void(^reloadCallBack)(void);
@end

NS_ASSUME_NONNULL_END
