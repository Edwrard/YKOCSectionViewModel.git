//
//  YKOCSectionViewModel.h
//  YKOCSectionViewModel
//
//  Created by linghit on 2022/1/17.
//

#ifndef YKOCSectionViewModel_h
#define YKOCSectionViewModel_h

#if __has_include("YKCoreSectionCollectionView.h")
#import <YKOCSectionViewModel/YKCoreSectionCollectionView.h>
#endif

#if __has_include("YKSectionCollectionView.h")
#import <YKOCSectionViewModel/YKSectionCollectionView.h>
#endif

#endif /* YKOCSectionViewModel_h */
