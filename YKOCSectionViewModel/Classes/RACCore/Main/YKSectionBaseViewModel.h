//
//  LMSectionBaseViewModel.h
//  AFNetworking
//
//  Created by Arc Lin on 2021/2/8.
//  Copyright © 2021 广东灵机文化传播有限公司（本内容仅限于广东灵机文化传播有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <ReactiveObjC/ReactiveObjC.h>
#import <YKSectionViewModelProtocol.h>
#import <YKSectionMainViewModelProtocol.h>
typedef void(^YKReloadDataCallback)(void);
typedef NSString * YKReuseViewType;

static YKReuseViewType _Nonnull className = @"class";
static YKReuseViewType _Nonnull idName = @"id";

NS_ASSUME_NONNULL_BEGIN

@interface YKSectionBaseViewModel : NSObject<YKSectionMainViewModelProtocol>

/// 数据源<Sections<Rows>>
@property (nonatomic, strong, readonly) NSArray<NSArray *> *dataSources;

///  用来注册cell用的信息
@property (nonatomic, strong, readonly) NSArray<NSDictionary<YKReuseViewType,YKReuseViewType> *> *cellInfos;

/// 用来注册头部view的信息
@property (nonatomic, strong, readonly) NSArray<NSDictionary<YKReuseViewType,YKReuseViewType> *> *sectionHeaderViewInfos;

/// 用来注册底部部view的信息
@property (nonatomic, strong, readonly) NSArray<NSDictionary<YKReuseViewType,YKReuseViewType> *> *sectionFooterViewInfos;

/// 所有sectionViewModels
@property (nonatomic, strong, readonly) NSArray<id<YKSectionViewModelProtocol>> *viewModels;

/// 当前没有被隐藏的所有Section
@property (nonatomic, strong, readonly) NSArray<id<YKSectionViewModelProtocol>> *showingViewModels;

/// 刷新collectionView
@property (nonatomic, strong, readonly) RACSubject *reloadSubject;

/// 错误回调
@property (nonatomic, strong, readonly) RACSubject<NSError *> *errorSubject;

/** 刷新成功的回调 */
@property (nonatomic, strong) void(^reloadSuccessCallback)(NSInteger section,NSInteger dataCount);

/** 控制器跳转 */
@property (nonatomic, strong) void(^handleViewController)(UIViewController *controller,YKSectionCollectionViewPushType pushType);


@end

NS_ASSUME_NONNULL_END
