//
//  WASectionHeaderViewProtocol.h
//  WarmConsultion
//
//  Created by Arc Lin on 2020/12/14.
//  Copyright © 2020 广东灵机文化传播有限公司（本内容仅限于广东灵机文化传播有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

#ifndef YKSectionReuseViewProtocol_h
#define YKSectionReuseViewProtocol_h

#import <YKSectionReuseViewProtocol.h>

@class LMViewModel;

@protocol YKSectionReuseViewProtocol <NSObject>

@optional

/// for collectionView
- (void)loadDataWithViewModel:(id<YKSectionViewModelProtocol>)viewModel atIndexPath:(NSIndexPath *)indexPath;

/// for tableView header footer
- (void)loadDataWithViewModel:(id<YKSectionViewModelProtocol>)viewModel atSection:(NSInteger)section;

@end


#endif /* YKSectionReuseViewProtocol_h */
