//
//  WASectionViewModelProtocol.h
//  WarmConsultion
//
//  Created by Arclin on 2020/12/13.
//  Copyright © 2020 广东灵机文化传播有限公司（本内容仅限于广东灵机文化传播有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

#ifndef YKSectionViewModelProtocol_h
#define YKSectionViewModelProtocol_h

typedef NS_ENUM(NSUInteger, YKSectionCollectionViewPushType) {
    LMSectionCollectionViewPushTypePush,
    LMSectionCollectionViewPushTypePresent,
};

#import <YKSectionResuseViewInfo.h>

@class RACCommand;

@protocol YKSectionViewModelProtocol <NSObject>

#pragma mark - DataSource

- (NSArray<RACCommand *> *_Nullable)yk_executeCommands;

- (NSArray *_Nullable)yk_dataSource;

- (NSArray<YKSectionResuseViewInfo *> *_Nullable)yk_cellNames;

- (NSString *_Nullable)yk_cellIdAtIndexPath:(NSIndexPath *_Nonnull)indexPath;

@optional

- (YKSectionResuseViewInfo *_Nullable)yk_sectionHeaderName;

- (YKSectionResuseViewInfo *_Nullable)yk_sectionFooterName;

- (NSString *_Nullable)yk_sectionHeaderId;

- (NSString *_Nullable)yk_sectionFooterId;

- (NSString *_Nullable)yk_sectionTitle;

/// 当无数据源时是否需要展示header,默认false
- (BOOL)yk_showHeaderWhenNoData;

- (id _Nullable)yk_dataAtIndexPath:(NSIndexPath * _Nonnull)indexPath;

#pragma mark - CollectionView

/// for collectionView
- (CGSize)yk_sectionHeaderSizeWithCollectionViewWidth:(CGFloat)width;

/// for collectionView
- (CGSize)yk_sectionFooterSizeWithCollectionViewWidth:(CGFloat)width;

/// section的内边距
- (UIEdgeInsets)yk_sectionInset;

///  最小行距
- (CGFloat)yk_sectionMinimumLineSpacing;

/// 最小列距
- (CGFloat)yk_sectionMinimumInteritemSpacing;

// for collectionView
- (CGSize)yk_cellSizeWithCollectionViewWidth:(CGFloat)width atIndexPath:(NSIndexPath *_Nullable)indexPath;

#pragma mark - TableView

/// for tableview
- (CGFloat)yk_sectionHeaderHeight;

/// for tableview
- (CGFloat)yk_sectionHeaderEstimatedHeight;

/// for tableview
- (CGFloat)yk_sectionFooterHeight;

/// for tableview
- (CGFloat)yk_sectionFooterEstimatedHeight;

/// for tableView
- (CGFloat)yk_cellHeightAtIndexPath:(NSIndexPath *_Nonnull)indexPath;

/// for tableview
- (CGFloat)yk_cellEstimatedHeightAtIndexPath:(NSIndexPath *_Nonnull)indexPath;

#pragma mark - User Event

- (void)yk_didSelectItemAtIndexPath:(NSIndexPath *_Nonnull)indexPath complete:(void(^_Nullable)(UIViewController * _Nullable controller,YKSectionCollectionViewPushType type))complete;
 
- (BOOL)yk_handleRouterEvent:(NSString *_Nonnull)eventName userInfo:(NSDictionary *_Nullable)userInfo controllerEvent:(void(^_Nullable)(UIViewController * _Nullable controller,YKSectionCollectionViewPushType type))controllerEvent;

- (BOOL)yk_isRowSelectableAtIndexPath:(NSIndexPath *_Nonnull)indexPath;

@end

#endif /* YKSectionViewModelProtocol_h */
