//
//  LMSectionResuseViewInfo.h
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import <Foundation/Foundation.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKSectionResuseViewInfo : NSObject

/// View的类名
@property (nonatomic, copy, readonly) NSString *className;

/// View的id
@property (nonatomic, copy, readonly) NSString *reuseViewId;

+ (instancetype)infoWithClassName:(NSString *)name identifier:(NSString *)identifer;

@end

NS_ASSUME_NONNULL_END
