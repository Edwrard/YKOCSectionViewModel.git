//
//  LMSectionResuseViewInfo.m
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import "YKSectionResuseViewInfo.h"

@interface YKSectionResuseViewInfo()

/// View的类名
@property (nonatomic, copy) NSString *className;

/// View的id
@property (nonatomic, copy) NSString *reuseViewId;

@end

@implementation YKSectionResuseViewInfo

+ (instancetype)infoWithClassName:(NSString *)name identifier:(NSString *)identifer
{
    YKSectionResuseViewInfo *info = [[YKSectionResuseViewInfo alloc] init];
    info.className = name;
    info.reuseViewId = identifer;
    return info;
}

@end
