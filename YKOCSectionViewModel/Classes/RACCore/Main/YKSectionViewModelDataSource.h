//
//  YKSectionViewModelDataSource.h
//  Pods
//
//  Created by Arc Lin on 2021/2/3.
//

#ifndef YKSectionViewModelDataSource_h
#define YKSectionViewModelDataSource_h

@protocol YKSectionViewModelDataSource <NSObject>

- (void)yk_requestDataWithComplete:(void(^)(id responseObject,NSError *error))complete;

@end
#endif /* YKSectionViewModelDataSource_h */
