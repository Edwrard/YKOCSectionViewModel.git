//
//  LMSectionMainViewModelProtocol.h
//  Pods
//
//  Created by Arc Lin on 2021/2/6.
//  Copyright © 2021 广东灵机文化传播有限公司（本内容仅限于广东灵机文化传播有限公司内部传阅，禁止外泄以及用于其他的商业目的）. All rights reserved.
//

#ifndef YKSectionMainViewModelProtocol_h
#define YKSectionMainViewModelProtocol_h

#import <YKSectionViewModelProtocol.h>

@protocol YKSectionMainViewModelProtocol <NSObject>

- (instancetype)initWithSectionViewModels:(NSArray<id<YKSectionViewModelProtocol>> *)sectionViewModels;

/// 刷新数据源
- (void)reloadData;

/// 刷新某部分数据源
/// @param indexSet section下标
- (void)reloadDataAtSection:(NSIndexSet *)indexSet;

/// 重设ViewModel
/// 是否需要执行数据请求逻辑
- (void)resetSectionViewModels:(NSArray<id<YKSectionViewModelProtocol>> *)viewModels needRequest:(BOOL)needRequest;

/// 插入ViewModel
- (void)insertSectionViewModel:(id<YKSectionViewModelProtocol>)viewModel atIndex:(NSInteger)index;

/// 删除viewModel
- (void)deleteSectionViewModel:(id<YKSectionViewModelProtocol>)viewModel;

/// 获取cell的id
- (NSString *)cellIdAtIndexPath:(NSIndexPath *)indexPath;

/// 点击跳转的控制器
/// vc可能为空 push或者present
- (void)didSelectItemAtIndexPath:(NSIndexPath *)indexPath;

/// 处理响应链事件
/// @param eventName 事件名
/// @param userInfo 附带信息
- (BOOL)handleRouterEvent:(NSString *)eventName userInfo:(NSDictionary *)userInfo handleController:(void(^)(UIViewController * controller,YKSectionCollectionViewPushType pushType))handleControllerBlock;


@end

#endif /* YKSectionMainViewModelProtocol_h */
