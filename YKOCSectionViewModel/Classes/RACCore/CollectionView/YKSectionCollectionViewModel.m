//
//  LMCollectionViewModel.m
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import "YKSectionCollectionViewModel.h"

@interface YKSectionCollectionViewModel()

@end

@implementation YKSectionCollectionViewModel

- (CGSize)cellSizeWithCollectionViewWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPath
{
    return [self.showingViewModels[indexPath.section] yk_cellSizeWithCollectionViewWidth:width atIndexPath:indexPath];
}

- (NSString *)sectionHeaderIdAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.showingViewModels[indexPath.section] respondsToSelector:@selector(yk_sectionHeaderId)]) {
        NSString *sectionHeader = [self.showingViewModels[indexPath.section] yk_sectionHeaderId];
        if (sectionHeader.length > 0) {
            return sectionHeader;
        }
    }
    return nil;
}

- (CGSize)headerSizeWithCollectionViewWidth:(CGFloat)width section:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionHeaderSizeWithCollectionViewWidth:)]) {
        return [self.showingViewModels[section] yk_sectionHeaderSizeWithCollectionViewWidth:width];
    }
    return CGSizeZero;
}

- (NSString *)sectionFooterIdAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.showingViewModels[indexPath.section] respondsToSelector:@selector(yk_sectionFooterId)]) {
        NSString *sectionFooter = [self.showingViewModels[indexPath.section] yk_sectionFooterId];
        if (sectionFooter.length > 0) {
            return sectionFooter;
        }
    }
    return nil;
}

- (CGSize)footerSizeWithCollectionViewWidth:(CGFloat)width section:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionFooterSizeWithCollectionViewWidth:)]) {
        return [self.showingViewModels[section] yk_sectionFooterSizeWithCollectionViewWidth:width];
    }
    return CGSizeZero;
}

- (UIEdgeInsets)insetAtSection:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionInset)]) {
        return [self.showingViewModels[section] yk_sectionInset];
    }
    return UIEdgeInsetsZero;
}

- (CGFloat)minimumLineSpacingAtSection:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionMinimumLineSpacing)]) {
        return [self.showingViewModels[section] yk_sectionMinimumLineSpacing];
    }
    return 0;
}

- (CGFloat)minimumInteritemSpacingAtSection:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionMinimumInteritemSpacing)]) {
        return [self.showingViewModels[section] yk_sectionMinimumInteritemSpacing];
    }
    return 0;
}

@end
