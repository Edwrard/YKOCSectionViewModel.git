//
//  LMCollectionViewModel.h
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import <Foundation/Foundation.h>

#import <ReactiveObjC/ReactiveObjC.h>
#import <YKSectionBaseViewModel.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKSectionCollectionViewModel : YKSectionBaseViewModel

/// 获取cel的size
/// @param width collectionView宽度
- (CGSize)cellSizeWithCollectionViewWidth:(CGFloat)width atIndexPath:(NSIndexPath *)indexPath;

/// 获取头部的id
- (NSString *)sectionHeaderIdAtIndexPath:(NSIndexPath *)indexPath;

/// 获取底部的id
- (NSString *)sectionFooterIdAtIndexPath:(NSIndexPath *)indexPath;

/// 获取头部尺寸
/// @param width collectionView宽度
- (CGSize)headerSizeWithCollectionViewWidth:(CGFloat)width section:(NSInteger)section;

/// 获取底部尺寸
/// @param width collectionView宽度
- (CGSize)footerSizeWithCollectionViewWidth:(CGFloat)width section:(NSInteger)section;

/// 获取某个section的inset
- (UIEdgeInsets)insetAtSection:(NSInteger)section;

/// 获取某个section的行距
- (CGFloat)minimumLineSpacingAtSection:(NSInteger)section;

/// 获取某个section的列距
- (CGFloat)minimumInteritemSpacingAtSection:(NSInteger)section;

@end

NS_ASSUME_NONNULL_END
