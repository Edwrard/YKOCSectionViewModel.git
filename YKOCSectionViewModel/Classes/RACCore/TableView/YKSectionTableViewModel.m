//
//  LMSectionTableViewModel.m
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import "YKSectionTableViewModel.h"

#import "YKSectionViewModelProtocol.h"
#import "YKSectionResuseViewInfo.h"

@interface YKSectionTableViewModel()

@end

@implementation YKSectionTableViewModel

- (CGFloat)cellHeightAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.showingViewModels[indexPath.section] respondsToSelector:@selector(yk_cellHeightAtIndexPath:)]) {
        return [self.showingViewModels[indexPath.section] yk_cellHeightAtIndexPath:indexPath];
    }
    return UITableViewAutomaticDimension;
}

- (CGFloat)cellEstimatedHeightAtIndexPath:(NSIndexPath *)indexPath
{
    if([self.showingViewModels[indexPath.section] respondsToSelector:@selector(yk_cellEstimatedHeightAtIndexPath:)]) {
        return [self.showingViewModels[indexPath.section] yk_cellEstimatedHeightAtIndexPath:indexPath];
    }
    return 44;
}

- (NSString *)sectionHeaderIdAtSection:(NSInteger)section
{
    if([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionHeaderId)]) {
        NSString *sectionHeader = [self.showingViewModels[section] yk_sectionHeaderId];
        return sectionHeader;
    }
    return nil;
}

- (NSString *)sectionFooterIdAtSection:(NSInteger)section
{
    if([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionFooterId)]) {
        NSString *sectionFooter = [self.showingViewModels[section] yk_sectionFooterId];
        return sectionFooter;
    }
    return nil;
}

- (CGFloat)headerHeightAtSection:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionHeaderHeight)]) {
        return [self.showingViewModels[section] yk_sectionHeaderHeight];
    }
    return 0;
}

- (CGFloat)headerEstimatedHeightAtSection:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionHeaderEstimatedHeight)]) {
        return [self.showingViewModels[section] yk_sectionHeaderEstimatedHeight];
    }
    return 0;
}

- (CGFloat)footerHeightAtSection:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionFooterHeight)]) {
        return [self.showingViewModels[section] yk_sectionFooterHeight];
    }
    return 0;
}

- (CGFloat)footerEstimatedHeightAtSection:(NSInteger)section
{
    if ([self.showingViewModels[section] respondsToSelector:@selector(yk_sectionFooterEstimatedHeight)]) {
        return [self.showingViewModels[section] yk_sectionFooterEstimatedHeight];
    }
    return 0;
}

@end
