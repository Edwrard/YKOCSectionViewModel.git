//
//  LMSectionTableView.m
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import "YKSectionTableView.h"
#import "YKSectionTableViewModel.h"
#import "YKSectionReuseViewProtocol.h"
#import <ReactiveObjC/ReactiveObjC.h>

@interface YKSectionTableView()<UITableViewDataSource,UITableViewDelegate>

@property (nonatomic, strong) YKSectionTableViewModel *viewModel;

@property (nonatomic, strong) RACSubject *reloadSubject;

@property (nonatomic, strong) RACSubject *errorSubject;

@end

@implementation YKSectionTableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style sectionViewModels:(nonnull NSArray<id<YKSectionViewModelProtocol>> *)sectionViewModels {
    if (self = [super initWithFrame:frame style:style]) {
        [self setSectionViewModels:sectionViewModels];
        self.delegate = self;
        self.dataSource = self;
        self.backgroundColor = UIColor.clearColor;
        [self registerReuseViews];
    }
    return self;
}

#pragma mark - UITableViewDataSource

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.viewModel.dataSources.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return self.viewModel.dataSources[section].count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell<YKSectionReuseViewProtocol> *cell = [tableView dequeueReusableCellWithIdentifier:[self.viewModel cellIdAtIndexPath:indexPath]];
    if ([cell respondsToSelector:@selector(loadDataWithViewModel:atIndexPath:)]) {
        [cell loadDataWithViewModel:self.viewModel.showingViewModels[indexPath.section] atIndexPath:indexPath];
    } else {
#ifdef DEBUG
        //
        NSLog(@"❌ %@缺少对loadDataWithViewModel:atIndexPath:的实现",NSStringFromClass(cell.class));
#else
#endif
    }
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.viewModel didSelectItemAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.viewModel cellEstimatedHeightAtIndexPath:indexPath];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return [self.viewModel cellHeightAtIndexPath:indexPath];
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    NSString *headerName = [self.viewModel sectionHeaderIdAtSection:section];
    if (!headerName || headerName.length == 0) return UIView.new;
    UITableViewHeaderFooterView<YKSectionReuseViewProtocol> *headerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:headerName];
    if ([headerView respondsToSelector:@selector(loadDataWithViewModel:atSection:)]) {
        [headerView loadDataWithViewModel:self.viewModel.showingViewModels[section] atSection:section];
    } else {
#ifdef DEBUG
        //
        NSLog(@"❌ %@缺少对loadDataWithViewModel:atSection:的实现",headerName);
#else
#endif
    }
    return headerView;
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    NSString *footerName = [self.viewModel sectionFooterIdAtSection:section];
    if (!footerName || footerName.length == 0) return UIView.new;
    UITableViewHeaderFooterView<YKSectionReuseViewProtocol> *footerView = [tableView dequeueReusableHeaderFooterViewWithIdentifier:footerName];
    if ([footerView respondsToSelector:@selector(loadDataWithViewModel:atSection:)]) {
        [footerView loadDataWithViewModel:self.viewModel.showingViewModels[section] atSection:section];
    } else {
#ifdef DEBUG
        //
        NSLog(@"❌ %@缺少对loadDataWithViewModel:atSection:的实现",footerName);
#else
#endif
    }
    return footerView;
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForHeaderInSection:(NSInteger)section
{
    return [self.viewModel headerEstimatedHeightAtSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView estimatedHeightForFooterInSection:(NSInteger)section
{
    return [self.viewModel footerEstimatedHeightAtSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return [self.viewModel headerHeightAtSection:section];
}

- (CGFloat)tableView:(UITableView *)tableView heightForFooterInSection:(NSInteger)section
{
    return [self.viewModel footerHeightAtSection:section];
}

#pragma mark - ScrollViewDelegate

- (void)safeScrollViewDelegateInvokeWithSelector:(SEL)selector
{
    if ([self.scrollViewDelegate respondsToSelector:selector]) {
        [self.scrollViewDelegate performSelector:selector withObject:self];
    }
}

- (void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewDidScroll:)];
}

- (void)scrollViewDidZoom:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewDidScroll:)];
}

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewWillBeginDragging:)];
}

- (void)scrollViewWillEndDragging:(UIScrollView *)scrollView withVelocity:(CGPoint)velocity targetContentOffset:(inout CGPoint *)targetContentOffset {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillEndDragging:withVelocity:targetContentOffset:)]) {
        [self.scrollViewDelegate scrollViewWillEndDragging:scrollView withVelocity:velocity targetContentOffset:targetContentOffset];
    }
}

- (void)scrollViewDidEndDragging:(UIScrollView *)scrollView willDecelerate:(BOOL)decelerate
{
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndDragging:willDecelerate:)]) {
        [self.scrollViewDelegate scrollViewDidEndDragging:scrollView willDecelerate:decelerate];
    }
}

- (void)scrollViewWillBeginDecelerating:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewWillBeginDecelerating:)];
}

- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewDidEndDecelerating:)];
}

- (void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewDidEndScrollingAnimation:)];
}

- (nullable UIView *)viewForZoomingInScrollView:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(viewForZoomingInScrollView:)]) {
        return [self.scrollViewDelegate viewForZoomingInScrollView:scrollView];
    }
    return nil;
}

- (void)scrollViewWillBeginZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewWillBeginZooming:withView:)]) {
        [self.scrollViewDelegate scrollViewWillBeginZooming:scrollView withView:view];
    }
}

- (void)scrollViewDidEndZooming:(UIScrollView *)scrollView withView:(nullable UIView *)view atScale:(CGFloat)scale {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewDidEndZooming:withView:atScale:)]) {
        [self.scrollViewDelegate scrollViewDidEndZooming:scrollView withView:view atScale:scale];
    }
}

- (BOOL)scrollViewShouldScrollToTop:(UIScrollView *)scrollView {
    if ([self.scrollViewDelegate respondsToSelector:@selector(scrollViewShouldScrollToTop:)]) {
        return [self.scrollViewDelegate scrollViewShouldScrollToTop:scrollView];
    }
    return YES;
}

- (void)scrollViewDidScrollToTop:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewDidScrollToTop:)];
}

- (void)scrollViewDidChangeAdjustedContentInset:(UIScrollView *)scrollView {
    [self safeScrollViewDelegateInvokeWithSelector:@selector(scrollViewDidChangeAdjustedContentInset:)];
}


#pragma mark - Event

- (void)reloadViewModelData {
    [self.viewModel reloadData];
}

- (void)reloadViewModelDataAtSections:(NSIndexSet *)sections {
    [self.viewModel reloadDataAtSection:sections];
}

- (BOOL)handleRouterEvent:(NSString *)event userInfo:(NSDictionary *)userInfo {
    return [self.viewModel handleRouterEvent:event userInfo:userInfo handleController:self.handleViewController];
}

- (void)resetViewModels:(NSArray<id<YKSectionViewModelProtocol>> *)viewModels needRequest:(BOOL)needRequest {
    [self.viewModel resetSectionViewModels:viewModels needRequest:needRequest];
    [self registerReuseViews];
}

- (void)insertSectionViewModel:(id<YKSectionViewModelProtocol>)viewModel atIndex:(NSInteger)index {
    [self.viewModel insertSectionViewModel:viewModel atIndex:index];
    [self registerReuseViews];
}

- (void)deleteSectionViewModel:(id<YKSectionViewModelProtocol>)viewModel {
    [self.viewModel deleteSectionViewModel:viewModel];
    [self registerReuseViews];
}

- (void)registerReuseViews {
    for (NSDictionary *cellInfo in self.viewModel.cellInfos) {
        [self registerClass:NSClassFromString(cellInfo[className]) forCellReuseIdentifier:cellInfo[idName]];
    }
    for (NSDictionary *sectionInfo in self.viewModel.sectionHeaderViewInfos) {
        [self registerClass:NSClassFromString(sectionInfo[className]) forHeaderFooterViewReuseIdentifier:sectionInfo[idName]];
    }
    for (NSDictionary *sectionInfo in self.viewModel.sectionFooterViewInfos) {
        [self registerClass:NSClassFromString(sectionInfo[className]) forHeaderFooterViewReuseIdentifier:sectionInfo[idName]];
    }
}

#pragma mark - setter & getter

- (void)setSectionViewModels:(NSArray<id<YKSectionViewModelProtocol>> *)sectionViewModels {
    self.viewModel = [[YKSectionTableViewModel alloc] initWithSectionViewModels:sectionViewModels];
    @weakify(self);
    [[[self.viewModel.reloadSubject deliverOnMainThread] throttle:0.3] subscribeNext:^(id  _Nullable x) {
        @strongify(self);
        [self.reloadSubject sendNext:nil];
        [self reloadData];
    }];
    [self.viewModel.errorSubject subscribe:self.errorSubject];
}

- (void)setHandleViewController:(void (^)(UIViewController * _Nonnull, YKSectionCollectionViewPushType))handleViewController {
    _handleViewController = handleViewController;
    self.viewModel.handleViewController = handleViewController;
}

- (RACSubject *)reloadSubject {
    if (!_reloadSubject) {
        _reloadSubject = [RACSubject subject];
    }
    return _reloadSubject;
}

- (RACSubject *)errorSubject {
    if (!_errorSubject) {
        _errorSubject = [RACSubject subject];
    }
    return _errorSubject;
}

- (NSArray *)currentSectionViewModels {
    return self.viewModel.viewModels;
}

- (void)dealloc {
    
}

@end
