//
//  LMSectionTableView.h
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import <UIKit/UIKit.h>
#import "YKSectionViewModelProtocol.h"

@class RACSubject;

NS_ASSUME_NONNULL_BEGIN

@interface YKSectionTableView : UITableView

- (instancetype)initWithFrame:(CGRect)frame style:(UITableViewStyle)style sectionViewModels:(NSArray<id<YKSectionViewModelProtocol>> *)sectionViewModels;

/** ScrollView代理 */
@property (nonatomic, weak) id<UIScrollViewDelegate> scrollViewDelegate;

/// 刷新数据回调
@property (nonatomic, strong, readonly) RACSubject *reloadSubject;

/// 网络请求错误回调
@property (nonatomic, strong, readonly) RACSubject *errorSubject;

/// 当前的sectionViewModel
@property (nonatomic, strong, readonly) NSArray *currentSectionViewModels;

/** 控制器跳转 */
@property (nonatomic, strong) void(^handleViewController)(UIViewController *controller,YKSectionCollectionViewPushType pushType);

/// 刷新数据源
- (void)reloadViewModelData;

/// 刷新某部分数据源
- (void)reloadViewModelDataAtSections:(NSIndexSet *)sections;

/// 重设SectionViewModel
/// @param viewModels viewModel
- (void)resetViewModels:(NSArray<id<YKSectionViewModelProtocol>> *)viewModels needRequest:(BOOL)needRequest;

/// 插入sectionViewModel
/// @param viewModel viewModel
/// @param index index
- (void)insertSectionViewModel:(id<YKSectionViewModelProtocol>)viewModel atIndex:(NSInteger)index;

/// 删除sectionViewModel
/// @param viewModel ViewModel
- (void)deleteSectionViewModel:(id<YKSectionViewModelProtocol>)viewModel;

/// 处理响应链路由处理事件
- (BOOL)handleRouterEvent:(NSString *)event userInfo:(NSDictionary *)userInfo;

@end

NS_ASSUME_NONNULL_END
