//
//  LMSectionTableViewModel.h
//  LMSectionViewModel
//
//  Created by Arc Lin on 2021/2/2.
//

#import <Foundation/Foundation.h>

#import <YKSectionBaseViewModel.h>

#import <ReactiveObjC/ReactiveObjC.h>

NS_ASSUME_NONNULL_BEGIN

@interface YKSectionTableViewModel : YKSectionBaseViewModel

/// 获取cel的高度
- (CGFloat)cellHeightAtIndexPath:(NSIndexPath *)indexPath;

/// 获取cel的估高
- (CGFloat)cellEstimatedHeightAtIndexPath:(NSIndexPath *)indexPath;

/// 获取头部的id
- (NSString *)sectionHeaderIdAtSection:(NSInteger)section;

/// 获取底部的id
- (NSString *)sectionFooterIdAtSection:(NSInteger)section;

/// 获取头部高度
- (CGFloat)headerHeightAtSection:(NSInteger)section;

/// 获取头部估算高度
- (CGFloat)headerEstimatedHeightAtSection:(NSInteger)section;

/// 获取底部高度
- (CGFloat)footerHeightAtSection:(NSInteger)section;

/// 获取底部估算高度
- (CGFloat)footerEstimatedHeightAtSection:(NSInteger)section;

@end

NS_ASSUME_NONNULL_END
