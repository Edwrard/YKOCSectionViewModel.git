# YKOCSectionViewModel

[![CI Status](https://img.shields.io/travis/edward/YKOCSectionViewModel.svg?style=flat)](https://travis-ci.org/edward/YKOCSectionViewModel)
[![Version](https://img.shields.io/cocoapods/v/YKOCSectionViewModel.svg?style=flat)](https://cocoapods.org/pods/YKOCSectionViewModel)
[![License](https://img.shields.io/cocoapods/l/YKOCSectionViewModel.svg?style=flat)](https://cocoapods.org/pods/YKOCSectionViewModel)
[![Platform](https://img.shields.io/cocoapods/p/YKOCSectionViewModel.svg?style=flat)](https://cocoapods.org/pods/YKOCSectionViewModel)

## Example

To run the example project, clone the repo, and run `pod install` from the Example directory first.

## Requirements

## Installation

YKOCSectionViewModel is available through [CocoaPods](https://cocoapods.org). To install
it, simply add the following line to your Podfile:

```ruby
pod 'YKOCSectionViewModel'
```

## Author

edward, 534272374@qq.com

## License

YKOCSectionViewModel is available under the MIT license. See the LICENSE file for more info.
