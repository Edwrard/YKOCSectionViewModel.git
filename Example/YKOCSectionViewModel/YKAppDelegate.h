//
//  YKAppDelegate.h
//  YKOCSectionViewModel
//
//  Created by edward on 01/17/2022.
//  Copyright (c) 2022 edward. All rights reserved.
//

@import UIKit;

@interface YKAppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
