//
//  main.m
//  YKOCSectionViewModel
//
//  Created by edward on 01/17/2022.
//  Copyright (c) 2022 edward. All rights reserved.
//

@import UIKit;
#import "YKAppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([YKAppDelegate class]));
    }
}
